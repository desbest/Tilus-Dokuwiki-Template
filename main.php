<?php
/**
 * DokuWiki Tilus Template
 * Based on the starter template
 *
 * @link     http://dokuwiki.org/template:tilus
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/mobileinputsize.js"></script>
</head>

<body class="site <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">

        <div class="topbar"></div>
    <div class="topbar2"></div>
    <?php tpl_includeFile('header.html') ?>
    <div class="header"><div class="container">
         <h1><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
                <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
        <?php if ($conf['tagline']): ?>
            <p class="claim"><?php echo $conf['tagline'] ?></p>
        <?php endif ?>

        <ul class="a11y skip">
            <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
        </ul>
    </div></div>
    
    <div class="menubar">
        <div class="container">
            <div id="navcontainer">
            <ul id="navlist">
           <!-- SITE TOOLS -->
                <h3 class="a11y"><?php echo $lang['site_tools'] ?></h3>
                
                    <?php tpl_toolsevent('sitetools', array(
                        'recent'    => tpl_action('recent', 1, 'li', 1),
                        'media'     => tpl_action('media', 1, 'li', 1),
                        'index'     => tpl_action('index', 1, 'li', 1),
                    )); ?>
            </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row mt18">

            <!-- The checkbox input & label partner -->
            <input type="checkbox" class="hide" id="menutoggle">
            <label class="menutoggle" for="menutoggle"><span class="showsidebartext">Show sidebar</span><span class="hidesidebartext">Hide sidebar</span></label>


            <div class="column sidebar w25pc" style="min-height: 300px;">


                <div class="contentheader">&nbsp;</div>
                <div class="contentsubheader">Search</div>
                <div class="contenthere padhere blockygradient">
                    <?php tpl_searchform() ?>
                </div>
                <?php if ($showSidebar): ?>
                <div class="contentheader">&nbsp;</div>                    
                <div id="writtensidebar" class="contenthere blockygradient">
                    
                      <!-- ********** ASIDE ********** -->
                    
                    <?php tpl_includeFile('sidebarheader.html') ?>
                    <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                    <?php tpl_includeFile('sidebarfooter.html') ?>
                    <div class="clearer"></div>
                </div>
                <?php endif; ?>
               <!-- PAGE ACTIONS -->
                <?php if ($showTools): ?>
                    <div class="contentheader">Page Tools</div>
                    <div class="contentsubheader"></div>
                    
                    
                    <div class="contenthere padhere blockygradient">
                <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                <div class="ddblueblockmenu">
                <ul>
                    <?php tpl_toolsevent('pagetools', array(
                        'edit'      => tpl_action('edit', 1, 'li', 1),
                        'discussion'=> _tpl_action('discussion', 1, 'li', 1),
                        'revisions' => tpl_action('revisions', 1, 'li', 1),
                        'backlink'  => tpl_action('backlink', 1, 'li', 1),
                        'subscribe' => tpl_action('subscribe', 1, 'li', 1),
                        'revert'    => tpl_action('revert', 1, 'li', 1),
                        'top'       => tpl_action('top', 1, 'li', 1),
                    )); ?>
                </ul>
                </div></div>
                <?php endif; ?>


                 <!-- USER TOOLS -->
                <?php if ($conf['useacl'] && $showTools): ?>
                      <div class="contentheader">User Tools</div>
                    <div class="contentsubheader"></div>
                    
                    
                    <div class="contenthere padhere blockygradient">
                <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                <div class="ddblueblockmenu">
                <ul>
                        <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>
                            <?php
                                if (!empty($_SERVER['REMOTE_USER'])) {
                                    echo '<li class="user">';
                                    tpl_userinfo(); /* 'Logged in as ...' */
                                    echo '</li>';
                                }
                            ?>
                            <?php /* the optional second parameter of tpl_action() switches between a link and a button,
                                     e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
                            ?>
                            <?php tpl_toolsevent('usertools', array(
                                'admin'     => tpl_action('admin', 1, 'li', 1),
                                'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                                'profile'   => tpl_action('profile', 1, 'li', 1),
                                'register'  => tpl_action('register', 1, 'li', 1),
                                'login'     => tpl_action('login', 1, 'li', 1),
                            )); ?>
                        </ul>
                </div></div>
                <?php endif; ?>
            </div>
            <div class="column w75pc" style="min-height: 300px;">

                <!-- <div class="contentheader"></div> -->
                <!-- <div class="contentsubheader">Photoshop Tutorials</div> -->
                <?php html_msgarea() /* occasional error and info messages on top of the page */ ?>
                <!-- <div class="contenthere">
                 <div class="listingback">
                        <div class="row flex">
                            <div class="column w80px">
                                <img src="images/darktheme/stencilback.jpg" />
                            </div>
                            <div class="column sparewidth nofloatonmobile">
                                <a href="#"><strong>How to create a glossy web menu</strong></a>
                                <p class="smaller">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porttitor lacus vel eros luctus hendrerit. </p>
                                <p class="smaller2">greycobra.com
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                56 views
                            </p>
                            </div>
                            <div class="column w200pxruby">
                                <p class="smaller2"><a href="#">Rate 1/5</a></p>
                                <p class="smaller2"><a href="#">Bookmark</a></p>
                                <p class="smaller2"><a href="#">Report</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="listingback">
                        <div class="row flex">
                            <div class="column w80px">
                                <img src="images/darktheme/stencilback.jpg" />
                            </div>
                            <div class="column sparewidth nofloatonmobile">
                                <a href="#"><strong>How to create a glossy web menu</strong></a>
                                <p class="smaller">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porttitor lacus vel eros luctus hendrerit. </p>
                                <p class="smaller2">greycobra.com
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                56 views
                            </p>
                            </div>
                            <div class="column w200pxruby">
                                <p class="smaller2"><a href="#">Rate 1/5</a></p>
                                <p class="smaller2"><a href="#">Bookmark</a></p>
                                <p class="smaller2"><a href="#">Report</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="listingback">
                        <div class="row flex">
                            <div class="column w80px">
                                <img src="images/darktheme/stencilback.jpg" />
                            </div>
                            <div class="column sparewidth nofloatonmobile">
                                <a href="#"><strong>How to create a glossy web menu</strong></a>
                                <p class="smaller">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porttitor lacus vel eros luctus hendrerit. </p>
                                <p class="smaller2">greycobra.com
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                56 views
                            </p>
                            </div>
                            <div class="column w200pxruby">
                                <p class="smaller2"><a href="#">Rate 1/5</a></p>
                                <p class="smaller2"><a href="#">Bookmark</a></p>
                                <p class="smaller2"><a href="#">Report</a></p>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="contentheader">&nbsp;</div>
                <div class="contentsubheader">
                    <!-- BREADCRUMBS -->
                    <?php if($conf['breadcrumbs']){ ?>
                        <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
                    <?php } ?>
                    <?php if($conf['youarehere']){ ?>
                        <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
                    <?php } ?>
                </div>
                <div class="contenthere padhere blockygradient">

                <!-- ********** CONTENT ********** -->
                <div id="dokuwiki__content">
                    <?php tpl_flush() /* flush the output buffer */ ?>
                    <?php tpl_includeFile('pageheader.html') ?>

                    <div class="page">
                        <!-- wikipage start -->
                        <?php tpl_content() /* the main content */ ?>
                        <!-- wikipage stop -->
                        <div class="clearer"></div>
                    </div>

                    <?php tpl_flush() ?>
                    <?php tpl_includeFile('pagefooter.html') ?>
                </div><!-- /content -->

                </div>
            </div>
        </div> <!-- end row -->
        <div class="row mt32">
            <div class="contentheader">Footer</div>
            <div class="contentsubheader"><!-- Footer --></div>
            <div class="contenthere blockygradient">
                <div class="footercontent">
                    <!-- <div class="block first">
                    <h2>Pages</h2>
                    <ul class="footermenu">
                    <li class="page_item"><a href="">About</a></li>
                    <li class="page_item"><a href="">Feedback</a></li>
                    </ul>
                    </div>

                    <div class="block">
                    <h2>Recently</h2>
                    <ul class="footermenu">
                    <li><a href=""><span class="date">January 2020</span> Item One </a></li>
                    <li><a href=""><span class="date">February 2020</span> Item Two </a></li>
                    <li><a href=""><span class="date">March 2020</span> Item Three </a></li>
                    <li><a href=""><span class="date">April 2020</span> Item Four</a></li>

                    </ul>
                    </div>

                    <div class="block">
                    <h2>Categories</h2>
                    <ul class="footermenu">
                    <li class="cat-item"><a href="">Bulletin</a></li>
                    <li class="cat-item"><a href="">Collaboration</a></li>
                    <li class="cat-item"><a href="">Comprehension</a></li>
                    <li class="cat-item"><a href="">Data</a></li>
                    </ul>
                    </div> -->
                <div class="clear"></div> 
                <!-- </div->
                </div> -->
                <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
                <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>

                <p><a href="http://desbest.com" target="_blank">Tilus theme by desbest</a></p>
                </div>
                <?php tpl_includeFile('footer.html') ?>
            </div>
        </div>
    </div>
    <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>


    <?php /* with these Conditional Comments you can better address IE issues in CSS files,
             precede CSS rules by #IE8 for IE8 (div closes at the bottom) */ ?>
    <!--[if lte IE 8 ]><div id="IE8"><![endif]-->

    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>
    
</body>
</html>
