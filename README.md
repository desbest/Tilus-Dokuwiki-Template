# Tilus dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information and colour themes](http://dokuwiki.org/template:tilus)

![tilus theme light screenshot](https://i.imgur.com/GBtn4lb.png)

![tilus theme dark screenshot](https://i.imgur.com/JAW1pNv.png)
